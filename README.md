# cases
* parabolic ( sepals) - see image parabolic1.png below and [the c code in git repo](https://gitlab.com/adammajewski/SepalsOfCauliflower) or [commons](https://commons.wikimedia.org/wiki/File:Parabolic_sepals_for_internal_angle_1_over_1.png)
* [Siegel disc ](https://commons.wikimedia.org/wiki/File:Quadratic_Golden_Mean_Siegel_Disc_Average_Velocity_-_Gray.png#Compare_with) - see siegel.png image below
*  basin of superattracting fixed point - Boettcher domain



![parabolic1.png](parabolic1.png)  



![siegel.png](siegel.png)


## superattracting case


$` r =  a e^t `$  

$` a  =  r /  e^t `$  

[curves intersects !](https://math.stackexchange.com/questions/2579773/do-two-exponential-spirals-intersect)


# Invariant curve 

Curve is invariant for the map f ( evolution function ) if images of every point from the curve stay on that curve.  
* forward-invariant (f(A)=Af(A)=A)
* backward invariant
* totaly 

Invariant curve of an operation T is also said to be stable under T. 

# Spiral
* [Logarithmic spiral](https://en.wikipedia.org/wiki/Logarithmic_spiral)
* [an exponential spiral](http://www.physics.emory.edu/faculty/weeks//ideas/spiral2.html)
* [the equiangular spiral ](https://www.maa.org/sites/default/files/images/upload_library/23/picado/seashells/espiraleng.html)





## equation 

### in polar coordinate 


$`r = a* e^{b*\tau}`$  
$`\tau = \frac{ln(\frac {r}{a})}{b} `$

### in cartesian coordinates

equation:

```
x^2 + y^2 == E^(ArcTan[y/x] Cot[α] )
```
coordinate 
```
x = radius^t*cos(t)
y = radius^t*sin(t)
```

## description 
* [xahlee](http://xahlee.info/SpecialPlaneCurves_dir/EquiangularSpiral_dir/equiangularSpiral.html)

### Plus Magazine
[Plus Magazine](https://plus.maths.org/content/polar-power):


>Now let’s look at all points whose polar coordinates (r, t) satisfy the equation
> r = e^{t/ 5}, where e is the base of the natural logarithm, e = 2.71828....
> When t=0, we get r = e^0 = 1 
> So our shape contains the point with polar coordinates (1,0) (whose Cartesian coordinates happen to also be (1,0)). 
> The animation below shows the radial line corresponding to the angle t as it varies from 0 to 2pi. 
> The point p with coordinates (e^{t/5}, t) is marked too. 
> Again we see the beginning of a spiral but this time a different one.
> But there is another trick we can play here: we can allow the angle $\theta $ to become negative! 
> To find a point whose second polar coordinate is negative you measure the angle from the positive x-axis in the other direction: clockwise. 
> For example, a point with polar coordinates (r,-pi/2) lies on the negative part of the y-axis.
> What does this mean for our logarithmic spiral? As t moves from 0 towards negative infinity, the radial line given by t turns clockwise through one, two, three, and any number of turns

### Wolfram

[Logarithmic Spirals and Möbius Transformations](http://demonstrations.wolfram.com/LogarithmicSpiralsAndMoebiusTransformations/)
> A logarithmic spiral can be parametrized by z0*a^t
> , where z0 is the point in the complex plane corresponding to t = 0. 
> The complex parameter a determines the spiral's rate of growth. 
> When |a|> 1, the spiral curls outward with increasing positive t and tends to infinity. 
> Increasing negative values of t make the spiral curl inward toward the origin. 






## direction
* a spiral expanding counterclockwise ( positive angle)
* clockwise ( negative angle)

# code 
* [tex](https://tex.stackexchange.com/questions/142814/how-can-i-draw-a-spiral-that-gets-arbitrary-close-to-a-unit-circle)
* [python](https://swiftcoder.wordpress.com/2010/06/21/logarithmic-spiral-distance-field/)
* [Java Script](https://forum.unity.com/threads/logarithmic-spiral-texture-troubles.255544/)
* [C++](https://stackoverflow.com/questions/4289452/logarithmic-spiral-is-point-on-spiral-cartesian-coordinates)
* [Maxima CAS](s.mac)

## gawk
[Eric R. Weeks ](http://www.physics.emory.edu/faculty/weeks//ideas/spiral2.html)
> radius = a * exp(b * theta)
> (I used a=1.0, b=0.1 for the above picture)
> Made with gawk script:

```gawk
seq 0 0.002 1.5 | gawk ' {a=1.0;b=0.1;t=5*$1*2*3.14159;r=a*exp(b*t);x=r*cos(t);y=r*sin(t);print x,y} ' | psdraw -X -111 96 -111 96 -S 10 10 -l 0.1 > splog.ps
```


## python

```python
# https://swiftcoder.wordpress.com/2010/06/21/logarithmic-spiral-distance-field/
import math
 
def spiral(x, y, a=1.0, b=1.0):
  # calculate the target radius and theta
  r = math.sqrt(x*x + y*y)
  t = math.atan2(y, x)
 
  # early exit if the point requested is the origin itself
  # to avoid taking the logarithm of zero in the next step
  if (r == 0):
    return 0
 
  # calculate the floating point approximation for n
  n = (math.log(r/a)/b - t)/(2.0*math.pi)
 
  # find the two possible radii for the closest point
  upper_r = a * math.pow(math.e, b * (t + 2.0*math.pi*math.ceil(n)))
  lower_r = a * math.pow(math.e, b * (t + 2.0*math.pi*math.floor(n)))
 
  # return the minimum distance to the target point
  return min(abs(upper_r - r), abs(r - lower_r))
 
 # produce a PNM image of the result
if __name__ == '__main__':
  print 'P2'
  print '# distance field image for spiral'
  print '1000 1000'
  print '255'
  for i in range(-500, 500):
    for j in range(-500, 500):
      print '%3d' % min( 255, int(spiral(i, j, 1.0, 0.5)) ),
    print
    
```



# git
```
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/invariant_curves_on_the_dynamic_plane.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

# License

This project is licensed under the  Creative Commons Attribution-ShareAlike 4.0 International License - see the [LICENSE.md](LICENSE.md) file for details

# Open Source 
* [](https://opensource.guide/)
# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)


